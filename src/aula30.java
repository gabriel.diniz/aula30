public class aula30 {  // laço de repetição FOR II

    public static void main(String[] args) {

        for (
                int i = 0; // INICIALIZAÇÃO
                i <= 10; // EXPRESSÃO
                ++i  // ATUALIZAÇÃO

        ){
            System.out.println( i );  // imprimir i

            /*
            for(part1,part2,part3;)
            * part1: declaramos uma varíavel
            * part2: colocamos a condição para que o looping continue a executar ou seja terminado
            * part3: incrementamos a varíavel
            */
        }

    }
}
